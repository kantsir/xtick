class TicketVersionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    @ticket =  FactoryGirl.create(:ticket)
    @ticket.update_attributes!(name: Faker::Name.name)
    @ticket.update_attributes!(body: Faker::Lorem.sentence)
  end

  test 'Should ticket have history' do
    get :index, ticket_id: @ticket.id
    assert_response :success
    assert assigns(:versions)
    assert_equal(assigns(:versions).count, 1)
  end

end