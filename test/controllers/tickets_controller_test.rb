class TicketsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test 'should redirected to new ticket path for not signed in user' do
    get :index
    assert_redirected_to :new_ticket
  end

  test 'should get index for signed in user' do
    sign_in StaffMember.first
    get :index
    assert_response :success
    assert assigns(:tickets)
    sign_out StaffMember.first
  end

  test 'should tickets retrieve by status' do
    2.times { FactoryGirl.create(:ticket) }

    sign_in StaffMember.first
    get :index, {status: 'waiting_for_customer'}
    assert_response :success
    assert assigns(:tickets)
    assert_equal(assigns(:tickets).count, 2)
  end

  test 'should ticket will be created' do
    post :create, ticket: FactoryGirl.build(:ticket).attributes
    assert_redirected_to assigns(:ticket)
  end

  test 'should ticket will be not created' do
    attributes = FactoryGirl.build(:ticket).attributes
    attributes[:email] = 'invalid'
    post :create, ticket: attributes
    assert_response :success
  end

  test 'should ticket will be updated' do
    ticket = FactoryGirl.create(:ticket)
    ticket.name = 'New name'
    patch :update, {id: ticket.id, ticket: ticket.attributes}
    assert_redirected_to assigns(:ticket)
  end

  test 'should ticket will not be updated' do
    ticket = FactoryGirl.create(:ticket)
    ticket.email = 'invalid'
    patch :update, {id: ticket.id, ticket: ticket.attributes}
    assert_response :success
  end

end