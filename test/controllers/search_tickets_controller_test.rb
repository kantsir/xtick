class SearchTicketsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    Ticket.remove_all_from_index!
    @ticket =  FactoryGirl.create(:ticket)
    Sunspot.commit
  end

  test 'Search tickets' do
    sign_in StaffMember.first
    post :search, search:{phrase: @ticket.name}
    assert_response :success
    assert assigns(:tickets)
    assert assigns(:phrase)
    assert_equal(assigns(:tickets).count, 1)
  end

  test 'Search tickets not signed in' do
    post :search, search:{phrase: @ticket.name}
    assert_redirected_to :new_staff_member_session
  end

end