FactoryGirl.define do

  factory :ticket do
    name Faker::Name.name
    email Faker::Internet.email
    departure Faker::Commerce.department
    subject Faker::Lorem.sentence[0..49]
    body Faker::Lorem.sentence
  end

end