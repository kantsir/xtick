require 'test_helper'

class TicketTest < ActiveSupport::TestCase

  def setup
    Ticket.remove_all_from_index!
    @ticket =  FactoryGirl.create(:ticket)
    Sunspot.commit
  end

  test 'check validation' do
    assert_raise(ActiveRecord::RecordInvalid) {
      Ticket.create!
    }
    assert_raise(ActiveRecord::RecordInvalid, 'Email is invalid'){
       @ticket.update_attributes!(email: 'invalid')
    }
  end

  test 'check default status after create' do
    assert_equal(@ticket.status,  Status.waiting_for_staff)
  end

  test 'check is id generated' do
    pattern = /[A-Z]{3}-[0-9abcdef]{1,}-[A-Z]{3}-[0-9abcdef]{1,}-[A-Z]{3}/
    assert_match(pattern, @ticket.id)
  end

  test 'check is new version created changed' do
     actual = @ticket.versions.count
     @ticket.update_attributes!(status: Status.on_hold)
     #first history change
     assert_equal(actual, @ticket.versions.count-1)

     #no history change
     @ticket.update_attributes!(name: 'Test Name new')
     assert_equal(actual, @ticket.versions.count-1)

     #second history change
     @ticket.update_attributes!(staff_member: StaffMember.first)
     assert_equal(actual, @ticket.versions.count-2)
  end

  test 'check phrase search' do
    phrase = @ticket.subject.split(' ').first
    assert_not_empty(Ticket.search_by_phrase phrase)
  end

  test 'check search ticket by id ' do
    tickets = Ticket.search_by_phrase(@ticket.id)
    assert_equal(tickets.count, 1)
    assert_equal(@ticket, tickets.first)
  end


end
