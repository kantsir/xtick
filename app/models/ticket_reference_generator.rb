require 'securerandom'

module TicketReferenceGenerator

  def generate_reference
    Array.new(3) { letters_str(3) }.join("-#{hex_str}-")
  end

  def letters_str(number)
    [*('A'..'Z')].sample(number).join
  end

  def hex_str
    SecureRandom.hex
  end

end