class Ticket < ActiveRecord::Base
  include TicketReferenceGenerator

  self.primary_key = 'id'

  validates :subject, :name, :body, :status, :departure, presence: true
  validates :subject, length: { minimum: 3,  maximum: 50 }
  validates :body, length: { minimum: 1, maximum: 250 }
  validates :staff_comment, length: { maximum: 250 }
  validates :email, :name, presence: true
  validates :email, email: true

  before_create :set_ticket_id
  before_validation :set_default_status
  after_create :send_created_email
  after_save :send_updated_email

  belongs_to :status
  belongs_to :staff_member
  attr_accessor :customer_mode

  #Versions
  has_paper_trail(if: Proc.new { |t| t.can_save_new_version? })

  #SOLR
  searchable do
    text :id, :name, :subject, :body, :staff_comment, :departure
  end

  #Scopes by statuses
  Status.active.each{|status|
    scope status.code, -> {
      where(status: status)
    }
  }

  def self.search_by_phrase(phrase)
    Ticket.search { fulltext "#{phrase}" }.results
  end

  def self.retrieve(status_code)
    send(Status.check(status_code) ? status_code : Status::DEFAULT)
  end

  def can_save_new_version?
    self.status_id_changed? || self.staff_member_id_changed? || self.staff_comment_changed?
  end

  private
  def set_default_status
    self.status = Status.waiting_for_staff if status.blank? || customer_mode
  end

  def set_ticket_id
    begin
      self.id = generate_reference
    end while Ticket.exists?(:id => id)
  end

  def send_created_email
    TicketMailer.create(self).deliver_later
  end

  def send_updated_email
    TicketMailer.update(self).deliver_later if can_save_new_version?
  end

end

