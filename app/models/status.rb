class Status < ActiveRecord::Base

  DEFAULT = :waiting_for_staff

  CODES = [DEFAULT, :waiting_for_customer, :on_hold,
                    :canceled, :completed]

  scope :active, -> { where(active: true) }

  CODES.each do |code|
    define_singleton_method(code) do
      find_by(code: code)
    end
  end

  def self.active_values
    self.active.map { |status| [status.name, status.id] }
  end

  def self.check(status)
    return false if status.blank?
    CODES.include?(status.to_sym)
  end

end
