class TicketMailer < ApplicationMailer

  [:create, :update].each{ |method_name|
    define_method(method_name) do |ticket|
      @ticket = ticket
      subject = "Ticket #{method_name}d"
      mail(to: ticket.email, subject: subject)
    end
  }

end
