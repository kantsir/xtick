class SearchTicketsController < ApplicationController

  before_action :authenticate_staff_member!

  def search
    @phrase = search_params[:phrase]
    @tickets = Ticket.search_by_phrase(@phrase)
    render 'tickets/index'
  end

  private
  def search_params
    params.require(:search).permit([:phrase])
  end

end
