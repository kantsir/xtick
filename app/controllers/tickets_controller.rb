class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :previous, :next, :edit, :update, :destroy]

  respond_to :html

  def index
    return redirect_to new_ticket_path unless staff_member_signed_in?
    @tickets = Ticket.retrieve(params[:status_code])
  end

  def show
  end

  def new
    @ticket = Ticket.new
  end

  def edit
  end

  def create
    @ticket = Ticket.new(ticket_params)
    redirect_to(@ticket, notice: 'Ticket was successfully created.') and return if @ticket.save
    render :new
  end

  def update
    redirect_to(@ticket, notice: 'Ticket was successfully updated.') and return if @ticket.update_attributes(ticket_params)
    render :edit
  end

  private
    def set_ticket
      @ticket = Ticket.find(params[:id])
      @ticket.customer_mode = !staff_member_signed_in?
    end

    def permitted_params
      params = [:name, :email, :departure, :subject, :body]
      params.concat [:status_id, :staff_member_id, :staff_comment] if staff_member_signed_in?
      params
    end

    def ticket_params
      params.require(:ticket).permit(permitted_params)
    end
end
