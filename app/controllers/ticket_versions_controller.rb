class TicketVersionsController < ApplicationController
  before_action :set_ticket

  def index
    @versions = @ticket.versions
  end

  private
    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end
end
