class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def user_for_paper_trail
    staff_member_signed_in? ? current_staff_member.email : 'customer'
  end

  def info_for_paper_trail
    { ip: request.remote_ip }
  end
end
