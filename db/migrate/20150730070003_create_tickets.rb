class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets, id: false  do |t|
      t.string :id, null: false
      t.string :name
      t.string :email
      t.string :departure
      t.string :subject
      t.string :body
      t.string :staff_comment
      t.belongs_to :status, index: true
      t.timestamps null: false
    end

    add_index :tickets, :id, unique: true
  end
end
