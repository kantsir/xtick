class AddMemberToTickets < ActiveRecord::Migration
  def change
    add_reference :tickets, :staff_member, index: true
  end
end
