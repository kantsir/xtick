# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Status.create!(code: :waiting_for_staff, name: 'Waiting for Staff', active: true)
Status.create!(code: :waiting_for_customer, name: 'Waiting for Customer', active: true)
Status.create!(code: :on_hold, name: 'On Hold', active: true)
Status.create!(code: :canceled, name: 'Cancelled', active: true)
Status.create!(code: :completed, name: 'Completed', active: true)

StaffMember.create!(name:'Test user', :email => 'test@test.com', :password => '12345678', :password_confirmation => '12345678')